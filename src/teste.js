const btn = document.getElementById("calcular")

//medidas da parede
const altura = document.getElementById("altura")
const largura = document.getElementById('largura')

//variaveis
const janela = document.getElementById('janela')
const porta = document.getElementById('porta')
const main = document.getElementById('parede')

//resultado
const resultado = document.getElementById('resultado')


const paredes = []
var controle = 0

//medidas da janela, porta e quanto a tinta rende.
const atributos = {
    janela: 2.40,
    porta: 1.52,
    tinta: 5 
}


main.innerHTML = `Insira o valor da parede aaaaa ${controle + 1}`
controle += 1

btn.addEventListener('click', ()=>{
      

//cálculo de cada parede no html, e no fim o quanto de cada lata de tinta precisa.
        paredeAtual = areaCadaParede(janela, porta, altura, largura)
        if(paredeAtual === false){
            return
        }
        paredes.push(paredeAtual)
        janela.value = ''
        porta.value = ''
        altura.value = ''
        largura.value = ''
        
        main.innerHTML = `Insira o valor da parede ${controle + 1}`
        console.log(controle)
        if(controle == 4){
            calcular(paredes, atributos)
            paredes.splice(0, paredes.length)
            
            controle = 0
            main.innerHTML = `Insira o valor da parede ${controle + 1}`
            
            return
        }
        controle += 1
        
        
})

//cálculo de cada parede.
const areaCadaParede = (janela, porta = 0, altura, largura) => {

  let msg = ''
  
  const areaParede = (altura.value * largura.value != '') ? (altura.value*largura.value) : 0
  
  
//Regras de negócio e validações.

//parede não pode ter menos de 1 metro
  if(areaParede < 1){
    msg += 'Valor da area deve ser maior que 1m.'
    altura.value = ''
    largura.value = ''
    return false
  }

//parede não pode ter mais de 50 metros
  if(areaParede > 50){
    msg += 'Area da parede não pode ser maior que 50m'
    altura.value = ''
    largura.value = ''
  }


//mensagens de alerta caso o usuário forneça algum dos dados errado.
  if(msg != ''){
    alert(msg)
    return false
  }


//cálculo se a parede tiver janelas e portas.
    const totalPerifericos = (atributos.janela*janela.value) + (atributos.porta*porta.value)


//o total de janelas + portas não pode ter mais de 50% da área da parede.
    if(totalPerifericos > (areaParede/100)*50){
      alert('Sua parede é menor do que a soma da janela e porta.')
      janela.value = ''
      porta.value = ''
      return false
    }  
    
  
//se tiver janelas e portas, tem que subtrair da área da parede.
    const areaParedeFinal = areaParede - totalPerifericos
    
    if(porta != 0){
      const portaValida = altura.value + atributos.porta > atributos.porta + 0.30  
      console.log(portaValida)
      if(!portaValida){
        alert("Altura da porta inválido.")
        return false
      }
    }
    
    
    return areaParedeFinal;
    
    
  }


//cálculo de tintas.
function calcular(arrayParede, valores){
    const tintasParede = {
        valor1: 18*atributos.tinta,
        valor2: 3.6*atributos.tinta,
        valor3: 2.5*atributos.tinta, 
        valor4: 0.5*atributos.tinta,
    }
    
    //qtd necessária de cada lata de tinta
    const lataTinta = {
        lata18: 0,
        lata3:0,
        lata2:0,
        lata0:0 
    }
    var areaTotal = 0
    for(var c in arrayParede){
        
         areaTotal += parseInt(arrayParede)
    }
    console.log("bdsqafahbsdfhdah "+areaTotal)
    

    
    qtd_tinta = 0
    
    while((qtd_tinta + tintasParede.valor1) <= areaTotal){
        qtd_tinta += 18
        lataTinta.lata18 += 1
    }
    
    while((qtd_tinta + tintasParede.valor2) <= areaTotal){
        qtd_tinta += 3.6
        lataTinta.lata3 += 1
    }

    while((qtd_tinta + tintasParede.valor3) <= areaTotal){
        qtd_tinta += 2.5
        lataTinta.lata2 += 1
    } 
    
    while((qtd_tinta + tintasParede.valor4) <= areaTotal){
    qtd_tinta += 0.5
    lataTinta.lata0 += 1
    }

    console.log(JSON.stringify(lataTinta)+ ' Lata tinta')
    resultado.innerHTML = "Você tem area = " + areaTotal + "  E você vai precisar de lata de 18L= " + lataTinta.lata18 + " | lata de 3,6L= " + lataTinta.lata3 + " | lata de 2,5L= " + lataTinta.lata2 + " | lata de 0,5L= " + lataTinta.lata0; 
    janela.value = ''
    porta.value = ''
    altura.value = ''
    largura.value = ''
    paredes.splice(0, paredes.length)

    return false  
}
    
    

//é isso, obrigado pelo desafio.