<h1 align="center">Desafio Digital Republic</h1>

<p align="center">O que deve ser desenvolvido
Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L</p>

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Javascript.js](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript). 
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

### Clone este repositório
$ git clone <https://github.com/levinakayama/digitalrepublic>

### Arquivo HTML
O arquivo INDEX está dentro da pasta SRC, basta dar dois cliques que o arquivo irá rodar e poderá interagir com a aplicação.

### Autor
[![Linkedin Badge](https://img.shields.io/badge/-Levi-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/levi-nakayama-96295825/)](https://www.linkedin.com/in/levi-nakayama-96295825/) 
[![Gmail Badge](https://img.shields.io/badge/-levisn@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:levisn@gmail.com)](mailto:levisn@gmail.com)

